#!/bin/bash
MOBILEPROVISIONFile="$1"
IPAFILE="$2"

filename=$(basename "$IPAFILE" .ipa)
tempextracted="/tmp/N_extracted"$filename

unzip -qo "$IPAFILE" -d $tempextracted
APPLICATION=$(ls "$tempextracted"/Payload/)
rm -rf $tempextracted"/__MACOSX"
cp "$MOBILEPROVISIONFile" $tempextracted"/Payload/$APPLICATION/embedded.mobileprovision"
