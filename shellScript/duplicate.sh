#!/bin/bash
duplicateNum="$1"
IPAFILE="$2"

filename=$(basename "$IPAFILE" .ipa)
tempextracted="/tmp/N_extracted"$filename
APPLICATION=$(ls "$tempextracted"/Payload/)

APP_NAME=$(/usr/libexec/PlistBuddy -c "Print CFBundleDisplayName"  "$tempextracted/Payload/$APPLICATION/Info.plist")
BUNDLE_ID=$(/usr/libexec/PlistBuddy -c "Print CFBundleIdentifier"  "$tempextracted/Payload/$APPLICATION/Info.plist")

/usr/libexec/PlistBuddy -c "Add ::UIDeviceFamily:0 integer 1" "$tempextracted/Payload/$APPLICATION/Info.plist"
/usr/libexec/PlistBuddy -c "Add ::UIDeviceFamily:1 integer 2" "$tempextracted/Payload/$APPLICATION/Info.plist"
/usr/libexec/PlistBuddy -c "Set :MinimumOSVersion 8.0" "$tempextracted/Payload/$APPLICATION/Info.plist"

/usr/libexec/PlistBuddy -c "Set :CFBundleDisplayName $APP_NAME-$duplicateNum" "$tempextracted/Payload/$APPLICATION/Info.plist"
/usr/libexec/PlistBuddy -c "Set :CFBundleIdentifier $BUNDLE_ID-$duplicateNum" "$tempextracted/Payload/$APPLICATION/Info.plist"

rm -rf "$tempextracted/Payload/$APPLICATION/PlugIns"
rm -rf "$tempextracted/Payload/$APPLICATION/Watch"
